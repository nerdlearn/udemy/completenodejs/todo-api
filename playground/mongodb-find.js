const { MongoClient, ObjectID } = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp',(error, client)=>{
	if(error){
		return console.log('Unable to connect to mongodb server.');
	}
	console.log('Connected to mongodb server.');
	
	const db = client.db('TodoApp');

	//FIND BY NAME
	db.collection('Users').find({
		 name: 'Pedro'
		}).toArray().then((docs)=>{
		console.log('Users');
		console.log(JSON.stringify(docs, undefined, 2));
	}, (err)=>{
		console.log('Unable to fetch Users.', err);
	});

	/*
	//COUNT TODOS
	db.collection('Todos').find().count().then((count)=>{
		console.log('Todos count: ', count);
	}, (err)=>{
		console.log('Unable to fetch Todos.', err);
	});	
	*/

	/*
	//FIND BY ID
	db.collection('Todos').find({
		 _id: new ObjectID('5c3b27742ff9fb1cf49abde5')
		}).toArray().then((docs)=>{
		console.log('Todos');
		console.log(JSON.stringify(docs, undefined, 2));
	}, (err)=>{
		console.log('Unable to fetch Todos.', err);
	});
	*/
	
	/*
	//FIND NOT COMPLETED
	db.collection('Todos').find({completed: false}).toArray().then((docs)=>{
		console.log('Todos');
		console.log(JSON.stringify(docs, undefined, 2));
	}, (err)=>{
		console.log('Unable to fetch Todos.', err);
	});
	*/
	//client.close();
});