//STRUCTURED CALL
//const MongoClient = require('mongodb').MongoClient;

//DESTRUCTURED CALL
const { MongoClient, ObjectID } = require('mongodb');


// THIS IS OBJECT DESCTRUCTURING
var user = {name: 'Pedro', age: 40};
var {name} = user;
console.log(name);


var obj = new ObjectID();

console.log('obj: ',obj);

MongoClient.connect('mongodb://localhost:27017/TodoApp',(error, client)=>{
	if(error){
		return console.log('Unable to connect to mongodb server.');
	}
	console.log('Connected to mongodb server.');
	
	const db = client.db('TodoApp');
	
	/*db.collection('Todos').insertOne({
		text: 'Something to do',
		completed: false
	},(err, result)=>{
		if(err){
			return console.log('Error insertin todo into Mongo DB.', err);
		}
		console.log(JSON.stringify(result.ops, undefined, 2));
	});*/
	/*
	db.collection('Users').insertOne({
		name: 'Pedro',
		age: 40,
		location: 'Costa Rica'
	},(err, result)=>{
		if(err){
			return console.log('Error insertin todo into Mongo DB.', err);
		}
		console.log(JSON.stringify(result.ops, undefined, 2));
		console.log(JSON.stringify(result.ops[0]._id.getTimestamp(), undefined, 2));
	});
	*/
	
	client.close();
});