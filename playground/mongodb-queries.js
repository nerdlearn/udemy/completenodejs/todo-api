const {
  ObjectID
} = require('mongodb');

var {
  mongoose
} = require('./../server/db/mongoose');

var {
  Todo
} = require('./../server/db/models/todo');

var id = '5c3f8eca66bb612e6cfc701e';

if (!ObjectID.isValid(id)) {
  console.log('id not valid');
} else {

  Todo.find({
    _id: id
  }).then((todos) => {
    console.log('Todos: ', todos);
  });

  //Gets one (first found)
  Todo.findOne({
    _id: id
  }).then((todo) => {
    if (!todo) {
      return console.log('There is no todo returned in search for id: ', id);
    }
    console.log('Todoby id: ', todo);
  }).catch((e) => console.log(e));
}
