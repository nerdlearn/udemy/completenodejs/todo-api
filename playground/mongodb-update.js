const {
  MongoClient,
  ObjectID
} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, client) => {
  if (error) {
    return console.log('Unable to connect to mongodb server.');
  }
  console.log('Connected to mongodb server.');

  const db = client.db('TodoApp');

  /*
  db.collection('Todos').findOneAndUpdate({
    _id: new ObjectID("5c3b3685561eed864c012b06") //FILTER
  }, {
	  $set: {
		  completed: true // UPDATE
	  }
  }, {
	  returnOriginal: false //returns new values, if true returns what we found before updates
  }).then((result)=>{
	  console.log(result);
  });
  */
  
  db.collection('Users').findOneAndUpdate({
    name: 'Diana'
  }, {
	  $set: {
		  name: 'Diana2'
	  }, 
	  $inc: {
		  age: 1
	  } 
  }, {
	  returnOriginal: false //returns new values, if true returns what we found before updates
  }).then((result)=>{
	  console.log(result);
  }); 

  client.close();
});
