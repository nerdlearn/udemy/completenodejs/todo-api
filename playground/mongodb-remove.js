const {
  ObjectID
} = require('mongodb');

var {
  mongoose
} = require('./../server/db/mongoose');

var {
  Todo
} = require('./../server/db/models/todo');

Todo.remove({}).then((result) => {
  console.log(result);
});

Todo.findAndRemove({
  _id: '5c3fa610b5b52af22f875849'
}).then((todo) => {
  console.log(todo);
});
Todo.findByIdAndRemove('5c3fa610b5b52af22f875849').then((todo) => {
  console.log(todo);
});
