const {
  MongoClient,
  ObjectID
} = require('mongodb');

MongoClient.connect('mongodb://localhost:27017/TodoApp', (error, client) => {
  if (error) {
    return console.log('Unable to connect to mongodb server.');
  }
  console.log('Connected to mongodb server.');

  const db = client.db('TodoApp');

  // deleteMany

  db.collection('Todos').deleteMany({
    text: 'Eat lunch'
  }).then((result) => {
    console.log(result);
    //results.result.ok = 1
    //results.result.n = 3
  });

  // deleteOne will look for the first value and delete that
  db.collection('Todos').deleteOne({
    text: 'Eat lunch'
  }).then((result) => {
    console.log(result);
    //results.result.ok = 1
    //results.result.n = 1
  });

  // findOneAndDelete
  db.collection('Todos').findOneAndDelete({
    completed: false
  }).then((result) => {
    console.log(result);
    //results.lastErrorObject = {n : 1}
    //results.value = { what we just deleted}
  });

  // findOneAndDelete  BY ID
  db.collection('Todos').findOneAndDelete({
    _id: new ObjectID("fdfdafasdfa")
  }).then((result) => {
    console.log(result);
    //results.lastErrorObject = {n : 1}
    //results.value = { what we just deleted}
  });

  client.close();
});
