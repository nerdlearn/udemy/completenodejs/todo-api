const {SHA256} = require('crypto-js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

var password = '123abc!';

bcrypt.genSalt(10,(err, salt)=>{
	bcrypt.hash(password, salt, (err, hash)=>{
		console.log(hash);
	});
});

var hashedPassword = '$2a$10$x7GriOzsCfhTxRFIAWWlZOCE4F.Tl2lCkydxW0EPujD/bzA5kdew2';

bcrypt.compare(password, hashedPassword, (err, res)=>{
		console.log(res);
});


// JWT SECTION

var dataJwt = {
	id: 10
};
var tokenJwt = jwt.sign(dataJwt, 'password123');
console.log(`JWT Token: ${tokenJwt}`);

var jwtDecoded = jwt.verify(tokenJwt, 'password123');

console.log(`JWT Decoded: ${JSON.stringify(jwtDecoded)}`);



// CRYPTOJS SECTION

var message = 'I am a user today.';
var hash = SHA256(message).toString();

console.log(`Message: ${message}`);
console.log(`Hash: ${hash}`);

var data = {
	id: 4
};

var token = {
	data,
	hash: SHA256(JSON.stringify(data)+ 'somesecret').toString()
}

var resultHash = SHA256(JSON.stringify(data)+ 'somesecret').toString();

if(resultHash===token.hash){
	console.log('Data was not changed');
} else {
	console.log('Data was changed, do not trust.');
}