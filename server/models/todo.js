var mongoose = require('mongoose');

var Todo = mongoose.model('Todo', {
    text: {
      type: String,
      required: true,
      minlength: 1,
      trim: true //removes empty spaces front and back
    },
    completed: {
      type: Boolean,
    default:
      false
    },
    completedAt: {
      type: Number,
      decault: null
    },
	_creator:{
      type: mongoose.Schema.Types.ObjectId,
      required: true
	}
  });

module.exports = {
  Todo
};
