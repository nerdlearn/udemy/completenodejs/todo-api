const mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

var UserSchema = new mongoose.Schema({
    name: {
      type: String,
      required: false,
      minlength: 1,
      trim: true //removes empty spaces front and back
    },
    age: {
      type: Number,
      required: false
    },
    email: {
      type: String,
      required: true,
      minlength: 1,
      trim: true, //removes empty spaces front and back
      unique: true,
      validate: {
        validator: validator.isEmail,
        message: '{VALUE} is not a valid email'
      }
    },
    password: {
      type: String,
      required: true,
      minlength: 6
    },
    tokens: [{
        access: {
          type: String,
          required: true
        },
        token: {
          type: String,
          required: true
        }
      }
    ]

  });

//statics go to Model, not to instance, so User, not user
UserSchema.statics.findByToken = function (token) {
  var User = this; // not instance user but rather model User
  var decoded;
  try {
    decoded = jwt.verify(token, process.env.JWT_SECRET);
  } catch (e) {
    return Promise.reject();
  }

  return User.findOne({
    '_id': decoded._id,
    'tokens.token': token,
    'tokens.access': 'auth'
  });
};

//finds a user by email
UserSchema.statics.findByCredentials = function (email, password) {
  var User = this; // not instance user but rather model User

  return User.findOne({
    email
  }).then((user) => {
    if (!user) {
      return Promise.reject();
    }
    //bcrypt only uses callbacks, so wrap in a Promise to keep things consistent
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, res) => {
        if (res) {
          resolve(user);
        } else {
          reject();
        }
      });
    });
  })
};

UserSchema.methods.generateAuthToken = function () {
  var user = this; // "this" being UserSchema user for this example
  var access = 'auth';
  var token = jwt.sign({
      _id: user._id.toHexString(),
      access
    }, process.env.JWT_SECRET).toString();

  user.tokens = user.tokens.concat([{
          access,
          token
        }
      ]);

  return user.save().then(() => {
    return token;
  })
};

UserSchema.methods.removeToken = function (token) {
  var user = this; // "this" being UserSchema user for this example

  return user.update({
    $pull: {
      tokens: {
        token
      }
    }
  });
};

// Override default toJSON so as to not return secure values like password or tokens
UserSchema.methods.toJSON = function () {
  var user = this; //
  var userObject = user.toObject();
  return _.pick(userObject, ['_id', 'email']);
};

// Execute previously to the save function of object
// make sure password is properly hashed
UserSchema.pre('save', function (next) {
  var user = this;

  if (user.isModified('password')) {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return err;
      }
      bcrypt.hash(user.password, salt, (err, hash) => {
        if (err) {
          return err;
        }
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

var User = mongoose.model('User', UserSchema);

module.exports = {
  User
};
