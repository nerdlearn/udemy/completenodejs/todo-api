const request = require('supertest');
const expect = require('expect');
const {
  ObjectID
} = require('mongodb');

const {
  app
} = require('./../server.js');

const {
  Todo
} = require('./../models/todo');

const {
  User
} = require('./../models/user');

const {
  todos,
  populateTodos,
  users,
  populateUsers
} = require('./seed/seed');

beforeEach(populateUsers);
beforeEach(populateTodos);

describe('POST /todos', () => {
  it('should create a new todo', (done) => {
    var text = 'Test todo text';

    request(app)
    .post('/todos')
    .set('x-auth', users[0].tokens[0].token)
    .send({
      text
    })
    .expect(200) // Success code
    .expect((res) => {
      expect(res.body.text).toBe(text);
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.find().then((todos) => {
        expect(todos.length).toBe(3);
        expect(todos[2].text).toBe(text);
        done();
      }).catch((e) => done(e));
    });
  });

  it('should not create a new todo with text data as empty string', (done) => {
    var text = '';

    request(app)
    .post('/todos')
    .set('x-auth', users[0].tokens[0].token)
    .send({
      text
    })
    .expect(400) // Failure code
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.find().then((todos) => {
        expect(todos.length).toBe(2);
        done();
      }).catch((e) => done(e));
    });
  });

  it('should not create a new todo with text data null', (done) => {
    request(app)
    .post('/todos')
    .set('x-auth', users[0].tokens[0].token)
    .send()
    .expect(400) // Failure code
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.find().then((todos) => {
        expect(todos.length).toBe(2);
        done();
      }).catch((e) => done(e));
    });
  });
});

describe('GET /todos', () => {
  it('should get all todos for a single user', (done) => {
    request(app)
    .get('/todos')
    .set('x-auth', users[0].tokens[0].token)
    .expect(200)
    .expect((res) => {
      expect(res.body.todos.length).toBe(1);
    })
    .end(done);
  });
});

describe('GET /todos/:id', () => {
  it('should return todo doc for an authorized user', (done) => {
    request(app)
    .get(`/todos/${todos[0]._id.toHexString()}`)
    .set('x-auth', users[0].tokens[0].token)
    .expect(200)
    .expect((res) => {
      expect(res.body.todo.text).toBe(todos[0].text);
	  expect(res.body.todo._creator).toBe(users[0]._id.toHexString());
    })
    .end(done);
  });
  
  it('should not return todo doc for an unauthorized user', (done) => {
    request(app)
    .get(`/todos/${todos[0]._id.toHexString()}`)
    .set('x-auth', '!thisIsAFakeToken$%$')
    .expect(401)
    .end(done);
  });
  
  it('should return 404 if todo not found', (done) => {
    request(app)
    .get(`/todos/${new ObjectID()}`)
    .set('x-auth', users[0].tokens[0].token)
    .expect(404)
    .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    request(app)
    .get(`/todos/${todos[0]._id.toHexString()}1`)
    .set('x-auth', users[0].tokens[0].token)
    .expect(404)
    .end(done);
  });
});

describe('DELETE /todos/:id', () => {
  it('should delete todo doc from authorized user', (done) => {
    var hexID = todos[0]._id.toHexString();
    request(app)
    .delete(`/todos/${hexID}`)
    .set('x-auth', users[0].tokens[0].token)
    .expect(200)
    .expect((res) => {
      expect(res.body.todo._id).toBe(hexID);
	  expect(res.body.todo._creator).toBe(users[0]._id.toHexString());
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.findById(hexID).then((todo) => {
        expect(todo).toBeFalsy();
        done();
      }).catch((e) => done(e));
    });
  });
  
  it('should return 401 and not delete todo doc from an unauthorized user', (done) => {
    var hexID = todos[0]._id.toHexString();
    request(app)
    .delete(`/todos/${hexID}`)
    .set('x-auth', '!thisIsAFakeToken$%$')
    .expect(401)
    .end(done);
  });

  it('should return 404 if todo not found', (done) => {
    request(app)
    .delete(`/todos/${new ObjectID()}`)
    .set('x-auth', users[0].tokens[0].token)
    .expect(404)
    .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    request(app)
    .delete(`/todos/${todos[0]._id.toHexString()}1`)
    .set('x-auth', users[0].tokens[0].token)
    .expect(404)
    .end(done);
  });
});

describe('PATCH /todos/:id', () => {
  it('should return 401 and not update todo text from unauthorized user', (done) => {
    var hexID = todos[0]._id.toHexString();
    var newTodoText = 'I really need todo this';
    request(app)
    .patch(`/todos/${hexID}`)
	.set('x-auth', '!thisIsAFakeToken$%$')
    .send({
      text: newTodoText
    })
    .expect(401)
    .end(done);
  });
  
  it('should update todo text from authorized user', (done) => {
    var hexID = todos[0]._id.toHexString();
    var newTodoText = 'I really need todo this';
    request(app)
    .patch(`/todos/${hexID}`)
	.set('x-auth', users[0].tokens[0].token)
    .send({
      text: newTodoText
    })
    .expect(200)
    .expect((res) => {
      expect(res.body.todo.text).toBe(newTodoText);
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.findById(hexID).then((todo) => {
        expect(todo.text).toBe(newTodoText);
        done();
      }).catch((e) => done(e));
    });
  });

  it('should update todo is completed = true and completedAt date not null  from authorized user', (done) => {
    var hexID = todos[0]._id.toHexString();
    request(app)
    .patch(`/todos/${hexID}`)
	.set('x-auth', users[0].tokens[0].token)
    .send({
      completed: true
    })
    .expect(200)
    .expect((res) => {
      expect(res.body.todo.completed).toBe(true);
      expect(typeof res.body.todo.completedAt).toBe('number');
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.findById(hexID).then((todo) => {
        expect(todo.completed).toBe(true);
        expect(typeof todo.completedAt).toBe('number');
        done();
      }).catch((e) => done(e));
    });
  });

  it('should update todo is completed = false and completedAt date null', (done) => {
    var hexID = todos[0]._id.toHexString();
    request(app)
    .patch(`/todos/${hexID}`)
	.set('x-auth', users[0].tokens[0].token)
    .send({
      completed: false
    })
    .expect(200)
    .expect((res) => {
      expect(res.body.todo.completed).toBe(false);
      expect(res.body.todo.completedAt).toBe(null);
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      Todo.findById(hexID).then((todo) => {
        expect(todo.completed).toBe(false);
        expect(todo.completedAt).toBe(null);
        done();
      }).catch((e) => done(e));
    });
  });

  it('should return 404 if todo not found', (done) => {
    request(app)
    .patch(`/todos/${new ObjectID()}`)
	.set('x-auth', users[0].tokens[0].token)
    .expect(404)
    .end(done);
  });

  it('should return 404 for non-object ids', (done) => {
    request(app)
    .patch(`/todos/${todos[0]._id.toHexString()}1`)
	.set('x-auth', users[0].tokens[0].token)
    .expect(404)
    .end(done);
  });
});

describe('GET /users/me', () => {
  it('should return a user if authenticated', (done) => {
    request(app)
    .get('/users/me')
    .set('x-auth', users[0].tokens[0].token)
    .expect(200)
    .expect((res) => {
      expect(res.body._id).toBe(users[0]._id.toHexString());
      expect(res.body.email).toBe(users[0].email);
    })
    .end(done);
  });

  it('should return 401 if not authenticated', (done) => {
    request(app)
    .get('/users/me')
    .set('x-auth', users[0].tokens[0].token + '1')
    .expect(401)
    .end(done);
  });
});

describe('POST /users', () => {
  it('should create a user', (done) => {
    var email = 'newUser@mynewuser.com';
    var password = 'myNewPassword!';

    request(app)
    .post('/users')
    .send({
      email,
      password
    })
    .expect(200) // Success code
    .expect((res) => {
      expect(res.headers['x-auth']).toBeTruthy();
      expect(res.body._id).toBeTruthy();
      expect(res.body.email).toBeTruthy();
      expect(res.body.email).toBe(email);
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      User.findOne({
        email
      }).then((user) => {
        expect(user).toBeTruthy();
        expect(user.password).not.toBe(password);
        expect(user.email).toBe(email);
        done();
      }).catch((e) => done(e));
    });
  });

  it('should return validation errors of request is invalid', (done) => {
    var email = '';
    var password = '';

    request(app)
    .post('/users')
    .send({
      email,
      password
    })
    .expect(400) // Failure code
    .end(done);
  });

  it('should not create user if email in use', (done) => {
    var email = users[0].email; //repeated email from seed
    var password = 'mydNewPassword!';

    request(app)
    .post('/users')
    .send({
      email,
      password
    })
    .expect(400) // Failure code
    .end(done);
  });
});

describe('POST /users/login', () => {
  it('should login valid user and return auth token', (done) => {
    var email = users[1].email;
    var password = users[1].password;

    request(app)
    .post('/users/login')
    .send({
      email,
      password
    })
    .expect(200) // Success code
    .expect((res) => {
      expect(res.headers['x-auth']).toBeTruthy();
      expect(res.body._id).toBeTruthy();
      expect(res.body.email).toBeTruthy();
      expect(res.body.email).toBe(email);
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      User.findById(users[1]._id).then((user) => {
        expect(user).toBeTruthy();
        expect(JSON.stringify(user.tokens[0]).replace('{', '').replace('}', '')).toMatch(JSON.stringify({
            access: 'auth',
            token: res.headers['x-auth']
          }).replace('{', '').replace('}', ''));
        done();
      }).catch((e) => done(e));
    });
  });

  it('should return validation errors of request is invalid', (done) => {
    var email = '';
    var password = '';

    request(app)
    .post('/users/login')
    .send({
      email,
      password
    })
    .expect(400) // Failure code
    .end(done);
  });
});

describe('DELETE /users/me/token', () => {
  it('should return 401 if not authenticated', (done) => {
    request(app)
    .delete('/users/me/token')
    .set('x-auth', users[0].tokens[0].token + ' 1 ')
    .expect(401)
    .end(done);
  });
  it(' should remove auth token on logout ', (done) => {
    request(app)
    .delete('/users/me/token ')
    .set('x-auth', users[0].tokens[0].token)
    .expect(200)
    .expect((res) => {
      expect(res.headers['x-auth']).toBeFalsy();
      expect(res.body.email).toBeFalsy();
    })
    .end((err, res) => {
      if (err) {
        return done(err);
      }

      User.findById(users[0]._id).then((user) => {
        expect(user).toBeTruthy();
        expect(user.tokens.length).toBe(0);
        done();
      }).catch((e) => done(e));
    })
  });
});
